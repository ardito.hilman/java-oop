/**
 * TestBook2
 */
public class TestBook2 {
    public static void main(String[] args) {

        // Declare and allocate an array of Authors
        Author[] authors = new Author[4];
        authors[0] = new Author("Tan Ah Teck", "AhTeck@somewhere.com", 'm');
        authors[1] = new Author("Paul Tan", "Paul@nowhere.com", 'm');
        authors[2] = new Author("Peter", "Peter@nowhere.com", 'm');
        authors[3] = new Author("Lowes", "Lowes@nowhere.com", 'f');

        // Declare and allocate a Book instance
        Book2 javaDummy = new Book2("Java for Dummy", authors, 19.99, 99);
        System.out.println(javaDummy);
        System.out.println("author name: "+javaDummy.getAuthorNames());
        System.out.println("author gender: "+javaDummy.getAuthorGender()); 
        System.out.println("author Email"+javaDummy.getAuthorEmail()); 
         // toString()
    }

}