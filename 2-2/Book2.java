import java.util.Arrays;

public class Book2 {
    // The private instance variables
    private String name;
    private Author[] authors;
    private double price;
    private int qty;

    /** Constructs a Book2 instance with the given author */
    public Book2(String name, Author[] authors, double price) {
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qty = 0;
     }
  
    /** Constructs a Book2 instance with the given author */
    public Book2(String name, Author[] authors, double price, int qty) {
       this.name = name;
       this.authors = authors;
       this.price = price;
       this.qty = qty;
    }
  
    // Getters and Setters
    /** Returns the name of this book2 */
    public String getName() {
       return name;
    }
    /** Return the Author instance of this book2 */
    public Author[] getAuthor() {
       return authors;  // return member author, which is an instance of the class Author
    }

    /** Returns the price */
    public double getPrice() {
       return price;
    }
    /** Sets the price */
    public void setPrice(double price) {
       this.price = price;
    }
    /** Returns the quantity */
    public int getQty() {
       return qty;
    }
    /** Sets the quantity */
    public void setQty(int qty) {
       this.qty = qty;
    }

    // get author name
    public String getAuthorNames() {
      String authorName = authors[0].getName();
      if (authors.length>1) 
         for (int i = 1; i < authors.length; i++) {
            authorName += ", "+authors[i].getName();
         }
      return authorName;
    }

    // get author email
    public String getAuthorEmail() {
      String authorEmail = authors[0].getEmail();
      if (authors.length>1) 
         for (int i = 1; i < authors.length; i++) {
            authorEmail += ", "+authors[i].getEmail();
         }
      return authorEmail;
    }

    //  get author gender
    public String getAuthorGender() {
      String authorGender = String.valueOf(authors[0].getGender());
      if (authors.length>1) 
         for (int i = 1; i < authors.length; i++) {
            authorGender += ", "+authors[i].getGender();
         }
      return authorGender;
    }
  
    /** Returns a self-descriptive String */
    public String toString() {
       return "Book2 [name ="+ name + ", by {" + Arrays.toString(authors)+ "}, price=" +price+ ", qty=" +qty+ "]";  // author.toString()
    }
}