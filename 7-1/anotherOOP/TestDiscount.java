package anotherOOP;
import java.util.Date;
public class TestDiscount {
    public static void main(String[] args) {
        Date d1 = new Date();
        Customer c1 = new Customer("Dimitrescu");
        c1.setMember(true);
        c1.setMemberType("premium");
        System.out.println(c1);

        Visit v1 = new Visit(c1, d1);
        v1.setServiceExpense(250);
        v1.setProductExpense(100);
        System.out.println(v1);

        Visit v2 = new Visit("Madam Miranda", d1);
        v2.setServiceExpense(100);
        v2.setProductExpense(200);
        System.out.println(v2);
    }
}
