package polimorph6;

public class Dog extends Animal{
    Dog(String name){
        super(name);
    }

    @Override
    public void greets() {
        System.out.println("woof");
    }

    public void greets(Dog another) {
        System.out.println("awoof");
    }
}
