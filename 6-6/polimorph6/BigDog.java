package polimorph6;

public class BigDog extends Dog{
    BigDog(String name){
        super(name);
    }

    @Override
    public void greets() {
        System.out.println("woow");
    }

    @Override
    public void greets(Dog another) {
        System.out.println("awoow");
    }

    public void greets(BigDog another) {
        System.out.println("awooooow");
    }
}
