package polimorph6;

public class Testpolimorph6 {
    public static void main(String[] args) {
        Cat c1 = new Cat("Hosico");
        Dog d1 = new Dog("Trace");
        Dog d2 = new Dog("Chop");
        BigDog d3 = new BigDog("Chop");
        BigDog d4 = new BigDog("Bert");

        c1.greets();
        d1.greets();
        d2.greets(d1);
        d3.greets();
        d3.greets(d1);
        d4.greets(d3);

    }
}
