package polimorph5;
public interface Resizeable {
    void resize(int percent);
}
