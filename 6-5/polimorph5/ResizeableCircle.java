package polimorph5;

public class ResizeableCircle extends Circle implements Resizeable {
    public ResizeableCircle(double radius){
        super(radius);
    }

    @Override
    public String toString() {
        return "ResizeableCircle["+super.toString()+"]";
    }

    @Override
    public void resize(int percent) {
        Double d= (double) percent;
        radius *= d/100;
    }
}
