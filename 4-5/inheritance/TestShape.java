package inheritance;

public class TestShape {
    public static void main(String[] args) {
        Shapes s1 = new Shapes();
        Shapes s2 = new Shapes("blue", false);
        System.out.println(s1);
        System.out.println(s2);

        Circle c1 = new Circle(7, "yellow", true);
        System.out.println(c1);
        System.out.println("Area: "+c1.getArea());
        System.out.println("Perimeter: "+c1.getPerimeter());

        Rectangle r1 = new Rectangle();
        r1.setWidth(4);
        r1.setLength(8);
        System.out.println(r1);
        System.out.println("area: "+r1.getArea());
        System.out.println("perimeter:  "+r1.getPerimeter());

        Square sq1 = new Square(10, "purple", true);
        System.out.println(sq1);
        System.out.println("area: "+sq1.getArea());
        System.out.println("perimeter:  "+sq1.getPerimeter());
    }
}
