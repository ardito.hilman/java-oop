package inheritance;
public class Shapes {
    private String color;
    private boolean filled;

    public Shapes(){
        color = "red";
        filled = true;
    }

    public Shapes(String color, boolean filled){
        this.color = color;
        this.filled = filled;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public String toString() {
        return "Shapes[Color= "+color+", filled = "+filled+"]";
    }
}
