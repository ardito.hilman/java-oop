package inheritance4;
public class TestMove {
    public static void main(String[] args) {
        Point p1 = new Point();
        Point p2 = new Point(2, 5);
        System.out.println(p1);
        System.out.println(p2);

        MoveablePoint mp1 = new MoveablePoint();
        MoveablePoint mp2 = new MoveablePoint(3, 3);
        MoveablePoint mp3 = new MoveablePoint(4, 6, 4, 6);
        System.out.println(mp1);
        System.out.println(mp2);
        System.out.println(mp3);
        mp1.setSpeed(10, 10);
        System.out.println("Move :"+mp1.move());
        System.out.println("Move :"+mp2.move());
        System.out.println("Move :"+mp3.move());
    }
}
