package polimorph8;
public class TestMove {
    public static void main(String[] args) {
        Moveable m1 = new MoveablePoint(5, 6, 10, 15);     // upcast
        System.out.println(m1);
        m1.moveLeft();
        System.out.println(m1);
        
        Moveable m2 = new MoveableCircle(1, 2, 3, 4, 20);  // upcast
        System.out.println(m2);
        m2.moveRight();
        System.out.println(m2);

        Moveable m3 = new MoveableRectangle(4, 6, 2, 1, 7, 7);  // upcast
        System.out.println(m3);
        m3.moveRight();
        System.out.println(m3);
    }
}
