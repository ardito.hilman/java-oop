package polimorph8;
public interface Moveable {
    void moveUp();
    void moveDown();
    void moveLeft();
    void moveRight();
} 
