package composition5;

public class Account{
    private int id;
    private Customer customer;
    private double balance;

    public Account(int id, Customer customer){
        this.id = id;
        this.customer =customer;
        balance = 0.0;
    }

    public Account(int id, Customer customer, double balance){
        this.id = id;
        this.customer =customer;
        this.balance = balance;
    }

    public int getID(){
        return id;
    }

    public Customer getCustomer(){
        return customer;
    }

    public double getBalance(){
        return balance;
    }

    public void setBalance(double balance){
        this.balance = balance;
    }

    public String getCustomerName(){
        return customer.getName();
    }

    public String toString(){
        return getCustomerName()+"("+id+") balance= $"+String.format("%.2f", balance)+"";    
    }

    public Account deposit(double amount){
        this.balance += amount;
        return this;
    }

    public Account withdraw(double amount){
        if (amount<=balance) {
            this.balance -= amount;
        } else {
            System.out.println("amount withdraw exceeded the curent balance");
        }
        return this;
    }

}