package polimorph4;
public interface Moveable {
    void moveUp();
    void moveDown();
    void moveLeft();
    void moveRight();
} 
