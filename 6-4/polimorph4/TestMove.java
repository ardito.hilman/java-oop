package polimorph4;
public class TestMove {
    public static void main(String[] args) {
        MoveablePoint mp1 = new MoveablePoint(0, 0, 5, 8);
        System.out.println(mp1);
        mp1.moveUp();
        System.out.println("move Up"+mp1);
        mp1.moveRight();
        System.out.println("move Right"+mp1);
        mp1.moveDown();
        System.out.println("move Down"+mp1);
        mp1.moveLeft();
        System.out.println("move Left"+mp1);

        MoveableCircle mc1 = new MoveableCircle(0, 0, 2, 3, 14);
        System.out.println(mc1);
        mc1.moveUp();
        System.out.println("move Up"+mc1);
        mc1.moveRight();
        System.out.println("move Right"+mc1);
        mc1.moveDown();
        System.out.println("move Down"+mc1);
        mc1.moveLeft();
        System.out.println("move Left"+mc1);
    }
}
