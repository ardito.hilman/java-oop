public class Employee{
    int id, salary;
    String firstName, lastName;

    public Employee(int id, String firstName, String lastName, int salary){
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
    }

    public int getID(){
        return id;
    }

    public String getFirstName(){
        return firstName;
    }

    public String getLastName(){
        return lastName;
    }

    public String getName(){
        return firstName+" "+lastName;
    }

    public int getSalary(){
        return salary;
    }

    public void setSalary(int newSalary){
        salary = newSalary;
    }

    public int getAnnualSalary(){
        return salary*12;
    }

    public int raiseSalary(int percent){
        float raise = salary*percent/100;
        int r = (int) Math.round(raise);
        salary += r;
        return salary;
    }

    public String toString() {
        return "Employee[id=" + id + " firstName=" + firstName + " lastName=" + lastName + " salary=" + salary + "]";
    }
}