public class Circle1 {  // Save as "Circle1.java"
   // private instance variable, not accessible from outside this class
   private double radius;
   
   // Constructors (overloaded)
   /** Constructs a Circle1 instance with default value for radius and color */
   public Circle1() {  // 1st (default) constructor
      radius = 1.0;
   }

   /** Constructs a Circle1 instance with the given radius and default color */
   public Circle1(double d) {  // 2nd constructor
        radius = d;
   }
   
   /** Returns the radius */
   public double getRadius() {
     return radius; 
   }

   //    Set radius
    public void setRadius(double d) {
        radius = d;
    }

   /** Returns the area of this Circle instance */
   public double getArea() {
      return radius*radius*Math.PI;
   }

    //return Circumference of Circle    
    public double getCircumference() {
         return 2*Math.PI*radius;
    }

    // to string
    public String toString() {
        return "Circle[radius=" + radius + "]";
     }

    public String getColor() {
        return null;
    }

    public void setColor(String string) {
    }

}