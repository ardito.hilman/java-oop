import java.util.Arrays;

public class MyIntStack {
    private int[] contents;
    private int tos;  // Top of the stack
 
   // constructors
    public MyIntStack(int capacity) {
        contents = new int[capacity];
        tos = -1;
    }
 
    public boolean push(int element) {
        // try {
            if (isFull()){
                contents = alocate();
                System.out.println("Stack overflow, increasing capacity");
                // throw new IllegalStateException("Stack Overflow, alocating data to another stack");
            }
            contents[++tos] = element;
            System.out.println(element + " pushed into stack");
            return true;
        // } catch (IllegalStateException e) {
        //     System.out.println(e);
        // }
    }

    public int[] alocate() {
        int [] newContent = new int[contents.length*2];
        for (int i = 0; i < contents.length; i++)
            newContent[i] = contents[i];
        return newContent;
    }

    public int pop() {
        if (isEmpty()) {
            System.out.println("Stack Underflow");
            return 0;
        }
        return contents[tos--];
    }
 
    public int peek() {
        return contents[tos];
    }
 
    public boolean isEmpty() {
        return tos < 0;
    }

    public boolean isFull() {
        return tos == contents.length - 1;
    }

    public String toString() {
        return "array1:"+ Arrays.toString(contents);
     }
}
