
package polimorph1;

public class Rectangle extends Shape{
    private double length, width;
    
    public Rectangle(){
        super();
        length = 1.0;
        width = 1.0;
    }

    public Rectangle(double width, double length){
        super();
        this.length = length;
        this.width = width;
    }

    public Rectangle(double width, double length, String color, boolean filled){
        super(color, filled);
        this.length = length;
        this.width = width;
    }

    public double getLength(){
        return length;
    }

    public void setLength(double Length){
        this.length = Length;
    }

    public double getWidth(){
        return width;
    }

    public void setWidth(double Width){
        this.width = Width;
    }

    @Override
    public double getArea(){
        return length*width;
    }

    @Override
    public double getPerimeter(){
        return 2*(length+width);
    }

    @Override
    public String toString() {
        return "Rectangle["+super.toString()+"width=" + width + " length=" + length + "]";
    }
}
